pref("extensions.verttabbar@frnchfrgg.org.description", "chrome://verttabbar/locale/verttabbar.properties");
pref("verttabbar.width", 250);
pref("verttabbar.position", "right");
pref("verttabbar.reverseLabels", false);
pref("verttabbar.swapButtons", false);
pref("verttabbar.tabmove", true);
pref("verttabbar.tabhighlight", false);
